# coding=utf

class HtmlOutputer(object):
	def __init__(self):
		self.datas = []

	def collect_data(self, data):
		if data is None:
			return
		self.datas.append(data)

	def output_html_bilibili_live(self):

		fout = open('F:\\bilibililive_spider\\output.html', 'w')

		fout.write("<html>")

		fout.write("<head>")
		fout.write("<meta charset= 'UTF-8'>")
		fout.write("</head>")

		fout.write("<body>")
		fout.write("<table>")
		fout.write("<tr>")
		fout.write("<td></td>")
		# fout.write("<td>%s</td>" % data['url'])
		fout.write("<td>主播</td>")
		fout.write("<td>标题</td>")
		fout.write("<td>是否正在直播</td>")
		fout.write("</tr>")
		count = 0
		for data in self.datas:
			count+=1
			fout.write("<tr>")
			fout.write("<td>%s</td>" % str(count))
			# fout.write("<td>%s</td>" % data['url'])
			fout.write("<td>%s</td>" % data['user_name'].encode('utf-8'))
			fout.write("<td>%s</td>" % data['title'].encode('utf-8'))
			fout.write("<td>%s</td>" % data['isBroad'])
			fout.write("</tr>")

		fout.write("</table>")
		fout.write("</body>")
		fout.write("</html>")

		fout.close()

	def output_html_baidu_zhidao(self):
		fout = open('F:\\baidu_zhidao_spider\\output.html', 'w')

		fout.write("<html>")

		fout.write("<head>")
		fout.write("<meta charset= 'UTF-8'>")
		fout.write("</head>")

		fout.write("<body>")
		fout.write("<table>")

		for data in self.datas:
			fout.write("<tr>")
			fout.write("<td>%s</td>" % data['url'])
			fout.write("<td>%s</td>" % data['content'].encode('utf-8'))
			# fout.write("<td>%s</td>" % data['summary'].encode('utf-8'))
			fout.write("</tr>")

		fout.write("</table>")
		fout.write("</body>")
		fout.write("</html>")

		fout.close()

	def output_html_foodmate(self):
		fout = open('F:\\foodmate_spider\\output.html', 'w')

		fout.write("<html>")

		fout.write("<head>")
		fout.write("<meta charset= 'UTF-8'>")
		fout.write("</head>")

		fout.write("<body>")
		fout.write("<table>")

		for data in self.datas:
			fout.write("<tr>")
			fout.write("<td>{0}</td>".format(data['title'].encode('utf-8')))
			fout.write("<td>")
			fout.write("<ul>")
			for content in data['content']:
				fout.write("<li>{0}</li>".format(content['info'].encode('utf-8')))
			fout.write("</ul>")
			fout.write("</td>")
			fout.write("</tr>")

		fout.write("</table>")
		fout.write("</body>")
		fout.write("</html>")

		fout.close()


	def output_txt_foodmate(self, type = 1):
		fout = open('type_{0}_data.txt'.format(type), 'w')
		info_title = []
		for data in self.datas:
			fout.write('{0}'.format(data['title'].strip().encode('utf-8')))
			for content in data['content']:
				info_list = info_value_splitter(content['info'])
				fout.write(',')
				fout.write('{0}'.format(info_list[1]))
				info_title.append(info_list[0])
			fout.write('\n')
		# for title in info_title:
		# 	fout.write(title.encode('utf-8'))
		# 	fout.write(',')
		fout.close()


def info_value_splitter(input_string):

	input_string = input_string.strip()
	string_list = input_string.split(')')
	string_list[0] = string_list[0] + ')'.encode('utf-8')
	return string_list


