# !/usr/bin/env python
# coding=utf-8

import cookie_manage,html_downloader,html_parser,url_manage,html_outputer


class SpiderMain(object):
    def __init__(self):
        self.cookies = cookie_manage.CookieManage()
        self.urls = url_manage.UrlManage()
        self.downloader = html_downloader.HtmlDownloader()
        self.parser = html_parser.HtmlParser()
        self.outputer = html_outputer.HtmlOutputer()

    def craw_manage(self, type, root_url, file_num = 1):
        if type == "baidu_zhidao":
            self.craw_baidu_zhidao(root_url)
        elif type == "bilibili_live":
            self.craw_bilibili_live(root_url)
        elif type == "foodmate":
            self.file_num = file_num
            self.craw_foodmate(root_url)
        else:
            pass

    def craw_baidu_zhidao(self, root_url):
        try:
            print "baidu_zhidao spider start"
            count = 1
            self.urls.add_new_url(root_url)
            while self.urls.has_new_url():
                try:
                    new_url = self.urls.get_new_url()
                    print 'craw %d : %s' %(count, new_url)
                    html_cont = self.downloader.download(new_url)
                    new_url, new_data = self.parser.parse_baidu_zhidao(new_url, html_cont)
                    for url in new_url:
                        self.urls.add_new_url(url)
                    self.outputer.collect_data(new_data)
                    if count == 2:
                        break
                    count += 1
                except:
                    print 'craw failed'
            self.outputer.output_html_baidu_zhidao()
            print "baidu_zhidao spider end"
        except Exception as e:
            print e.message

    def craw_bilibili_live(self, root_url):
        try:
            print "bilibili_live spider start"
            count = 1
            self.urls.add_new_url(root_url)
            while self.urls.has_new_url():
                try:
                    new_url = self.urls.get_new_url()
                    html_cont = self.downloader.download(new_url)
                    new_data = self.parser.parse_bilibili_live(new_url, html_cont)
                    self.urls.get_new_url(new_url)
                    self.outputer.collect_data(new_data)
                    if count >=  1:
                        break
                except:
                    print 'craw failed'
            self.outputer.output_html_bilibili_live()
            print "bilibili_live spider end"
        except Exception as e:
            print e.message

    def craw_foodmate(self, root_url):
        try:
            print "foodmate spider start"
            self.urls.add_new_url(root_url)
            new_url = self.urls.get_new_url()
            html_cont = self.downloader.download(new_url)
            new_url = self.parser.parse_foodmate(new_url, html_cont)
            for url in new_url:
                self.urls.add_new_url(url)

            while self.urls.has_new_url():
                try:
                    new_url = self.urls.get_new_url()
                    html_cont = self.downloader.download(new_url)
                    new_data = self.parser.parse_foodmate2(new_url, html_cont)
                    self.outputer.collect_data(new_data)
                except:
                    print 'craw failed'
            self.outputer.output_txt_foodmate(type = self.file_num)
            print "foodmate spider end"
        except Exception as e:
            print e.message

if __name__=="__main__":
    
    url_temp = "http://db.foodmate.net/yingyang/type_{0}.html"
    for i in range(1,22):
        obj_spider = SpiderMain()
        print("Crawlling on type {0}".format(i))
        obj_spider.craw_manage("foodmate", url_temp.format(i), file_num = i)


