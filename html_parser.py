# !/usr/bin/env python
# coding=utf-8

import re
import urlparse
from bs4 import BeautifulSoup

class HtmlParser(object):
	def __init__(self):
		pass

	def parse_baidu_zhidao(self, page_url, html_cont):
		if page_url is None or html_cont is None:
			return

		soup = BeautifulSoup(html_cont, 'html.parser', from_encoding="utf-8")
		new_urls = self._get_new_baidu_zhidao_urls(page_url, soup)
		new_data = self._get_new_baidu_zhidao_data(page_url, soup)
		return new_urls, new_data

	def _get_new_baidu_zhidao_urls(self, page_url, soup):
		new_urls = set()
		# /view/123.htm
		links = soup.find_all('a', href=re.compile(r"/view/\d+\.htm"))
		for link in links:
			new_url = link['href']
			new_full_url = urlparse.urljoin(page_url, new_url)
			new_urls.add(new_full_url)
		return new_urls

	def _get_new_baidu_zhidao_data(self, page_url, soup):
		res_data = {}
		res_data['url'] = page_url
		content = soup.find("div", id="text").find_all("table")
		res_data['content'] = content[2].get_text()
		return res_data


	def parse_bilibili_live(self, page_url, html_cont):
		if page_url is None or html_cont is None:
			return

		soup = BeautifulSoup(html_cont, 'html.parser', from_encoding="utf-8")
		# new_urls = self._get_new_urls(page_url, soup)
		new_data = self._get_new_data(page_url, soup)
		# return new_urls, new_data
		return new_data

	def _get_new_bilibili_live_urls(self, page_url, soup):
		new_urls =set()
		# /view/123.htm
		links = soup.find_all('a', href=re.compile(r"/view/\d+\.htm"))
		for link in links:
			new_url = link['href']
			new_full_url = urlparse.urljoin(page_url, new_url)
			new_urls.add(new_full_url)
		return new_urls

	def _get_new_bilibili_live_data(self, page_url, soup):
		res_data = {}
		res_data['url'] = page_url
		print soup
		# title_node = soup.find("dd", class_="lemmaWgt-lemmaTitle-title").find('h1')
		# res_data['title'] = title_node.get_text()
        #
		# summary_node = soup.find("div", class_="lemma-summary")
		# res_data['summary'] = summary_node.get_text()
		# print soup
		try:
			content = soup.find("i", class_="live-icon room-verified")
			res_data['user_name'] = content.attrs["data-remark"]
		except:
			res_data['user_name'] = ""

		try:
			content = soup.select("div.master-info span.info-text")[0]
			res_data['master'] = content.get_text()
		except:
			res_data['master'] = ""

		try:
			content = soup.find("h2", class_="dp-inline-block room-title")
			res_data['title'] = content.attrs["title"]
		except:
			res_data['title'] = ""


		# content = soup.find("div", class_="live-switcher")
		# content = soup.select("div.live-switcher")
		# print content
		# if content.get_text() == "1":
		# 	res_data['isBroad'] = "是"
		# else:
		# 	res_data['isBroad'] = "否"
		return res_data


	def parse_foodmate(self, page_url, html_cont):
		if page_url is None or html_cont is None:
			return

		soup = BeautifulSoup(html_cont, 'html.parser', from_encoding="utf-8")
		new_urls = self._get_new_foodmate_urls(page_url, soup)
		# new_data = self._get_new_foodmate_data(page_url, soup)
		return new_urls

	def parse_foodmate2(self, page_url, html_cont):
		if page_url is None or html_cont is None:
			return

		soup = BeautifulSoup(html_cont, 'html.parser', from_encoding="utf-8")
		new_data = self._get_new_foodmate_data(page_url, soup)
		return new_data

	def _get_new_foodmate_urls(self, page_url, soup):
		new_urls = set()
		# /type_0%3A1%3A0_1.html
		links = soup.find_all('a', href=re.compile(r"type_0.*\.html"))
		for link in links:
			new_url = link['href']
			new_full_url = urlparse.urljoin(page_url, new_url)
			new_urls.add(new_full_url)
		return new_urls

	def _get_new_foodmate_data(self, page_url, soup):
		res_data = {}
		res_data["title"] = soup.find("div", id="rightlist").b.string
		contents = soup.find_all('div', attrs={"class":"list"})
		res_data["content"] = []
		for content in contents:
			c = {}
			c["info"] = content.text
			res_data["content"].append(c)
		return res_data