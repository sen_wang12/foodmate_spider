# !/usr/bin/env python
# coding=utf-8

import os
import sys

class CookieManage(object):
    def __init__(self):
        pass

    def getBaiduCookie(self):
        try:
            dir = sys.path[0]
            filePath = os.path.join(dir,"settings.txt")
            file = open(filePath, 'r')
            cookie = file.read()
            return cookie
        except Exception as e:
            return
