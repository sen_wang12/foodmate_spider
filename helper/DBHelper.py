# -*- coding: utf-8 -*-
import MySQLdb
import MySQLdb.cursors

class DBHelper:
    host = 'localhost'
    user = 'root'
    passwd =''
    db = 'spider_bilibililive'

    #查询一条结果集
    @staticmethod
    def query_one(sql):
        cur = None
        try:
            sql = sql.encode('utf8')
            conn = MySQLdb.connect(
                host=DBHelper.host,
                user=DBHelper.user,
                passwd=DBHelper.passwd,
                db=DBHelper.db,
                cursorclass = MySQLdb.cursors.DictCursor
            )
            conn.set_character_set('utf8')
            cur = conn.cursor()
            count = cur.execute(sql)
            if count == 0:
                return None
            result = cur.fetchone() #查询一条
            cur.close()
            conn.close()
            return result
        except Exception as e:
            pass
        finally:
            if cur is not None:
                try:
                    cur.close()
                    conn.close()
                except:
                    pass

    @staticmethod
    def execute(sql):
        cur = None
        try:
            sql = sql.encode('utf8')
            conn = MySQLdb.connect(
                host=DBHelper.host,
                user=DBHelper.user,
                passwd=DBHelper.passwd,
                db=DBHelper.db,
                cursorclass = MySQLdb.cursors.DictCursor
            )
            conn.set_character_set('utf8')
            cur = conn.cursor()
            cur.execute(sql)
            conn.commit()
        except Exception as e:
            pass
        finally:
            if cur is not None:
                try:
                    cur.close()
                    conn.close()
                except:
                    pass

    #返回所有结果集--xgs
    @staticmethod
    def query_all(sql):
        cur = None
        try:
            sql = sql.encode('utf8')
            conn = MySQLdb.connect(
                host=DBHelper.host,
                user=DBHelper.user,
                passwd=DBHelper.passwd,
                db=DBHelper.db,
                cursorclass = MySQLdb.cursors.DictCursor
            )
            conn.set_character_set('utf8')
            cur = conn.cursor()
            count = cur.execute(sql)
            if count == 0:
                return None
            result = cur.fetchmany(count)   #查询所有
            cur.close()
            conn.close()
            return result
        except Exception as e:
            pass
        finally:
            if cur is not None:
                try:
                    cur.close()
                    conn.close()
                except:
                    pass





# cursor = conn.cursor()
#
# sql = "select * from user"
# cursor.execute(sql)
#
# print cursor.rowcount
#
# rs = cursor.fetchone()
# print rs
#
# rs = cursor.fetchmany(2)
# print rs
#
# rs = cursor.fetchall()
# print rs
#
# cursor.close()
# conn.close()
#
#
# #connect对象->arg func
# # host
# # port
# # user
# # passwd
# # db
# # charset
#
# # cursor()
# # commit()
# # rollback()
# # close()
# #
# #cursor对象-> func
# # execute(op[,args])
# # fetchone()
# # fetchmany(size)
# # fetchall()
# # rowcount
# # close()