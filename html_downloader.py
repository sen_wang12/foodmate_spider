# !/usr/bin/env python
# coding=utf-8

import urllib, urllib2
import json

class HtmlDownloader(object):
    def __init__(self):
        pass

    def download(self, url):
        if url is None:
            return None

        request = urllib2.Request(url)
        request.add_header('User-Agent', 'Mozilla/5.0 (Linux; Android 4.4.4; HM NOTE 1LTEW Build/KTU84P) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/33.0.0.0 Mobile Safari/537.36 MicroMessenger/6.0.0.54_r849063.501 NetType/WIFI')
        response = urllib2.urlopen(request)
        if response.getcode() != 200:
            return None
        return response.read()

    def download_baidu_teiba_mylike(self, cookie):
        url = "http://tieba.baidu.com/f/like/mylike?v=1476951317766"
        data = {}
        data = urllib.urlencode(data)
        cookie_in_head = "BDUSS={};".format(cookie)
        headers = {
            "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "Accept-Language":"zh-CN,zh;q=0.8",
            "Connection":"keep-alive",
            "Host":"tieba.baidu.com",
            "Referer":"https://www.baidu.com/",
            "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36",
            "Cookie": cookie_in_head,
        }

        req = urllib2.Request(url, data, headers)
        response = urllib2.urlopen(req)
        if response.getcode() != 200:
            return None

        the_page = response.read()

        return the_page

    def send_baidu_teiba_check(self, kw, cookie):
        url = "http://tieba.baidu.com/sign/add"
        data = {
            "ie":"utf-8",
            "kw":kw.encode('utf-8'),
            "tbs":"702f44f4be4010f71476951583"
        }
        data = urllib.urlencode(data)
        cookie_in_head = "BDUSS={};".format(cookie)
        headers = {
            "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "Accept-Language":"zh-CN,zh;q=0.8",
            "Connection":"keep-alive",
            "Host":"tieba.baidu.com",
            "Referer":"https://www.baidu.com/",
            "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36",
            "Cookie": cookie_in_head,
        }

        req = urllib2.Request(url, data, headers)
        response = urllib2.urlopen(req)
        if response.getcode() != 200:
            return "failed"
        else:
            return "success"



